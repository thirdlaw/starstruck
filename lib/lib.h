/***===========================================================***
 ***                                                           ***
 ***                  THIRD LAW ROBOTICS HAL                   ***
 ***                           libHAL                          ***
 ***                                                           ***
 ***===========================================================***
 *
 * This code is protected under the license available at:
 *      https://gitlab.com/thirdlaw/starstruck/blob/master/LICENSE
 *//**
 *
 * A general library for HAL to allow robot control and controller input
 *
 * See individual library components for more information and for their interface
 */

 /**
 * @file
 * @author mrRachar
 */ // For all file contents to be documented

 /**
  * @author mrRachar
  * @mainpage libHAL
  *
  * @section intro_sec Introduction
  *
  * libHAL is a general library for robot interaction, simplifying more complex robot
  * operations. It provides a functional interface for interacting with the robot, either
  * by motor control or giving the robot distances to move.
  *
  * @section design_sec Structure
  *
  * The library is split up into two sections:
  * - @ref rcs
  * - Autonomous Motion Control *under refinement*
  *
  * You can include "lib.h" from the library root to include all library functionalities,
  * or include "lib.h" in subfolders, or include individual files, to individual files for
  * only specific functionality.
  *
  * @section lic_sec License
  *
  * All the code here is covered by the special license available at:
  *      https://gitlab.com/thirdlaw/starstruck/blob/master/LICENSE
  */

  #include "rcs/lib.h"

  #ifndef LIB_LIB_H
  #define LIB_LIB_H
  // Any general library code may go here, if any

  // Import guard
  #endif
