/***===========================================================***
 ***                                                           ***
 ***                  THIRD LAW ROBOTICS HAL                   ***
 ***               R.C.S. (Robot Control System)               ***
 ***                           v0.1.0                          ***
 ***                                                           ***
 ***===========================================================***
 *
 * This code is protected under the license available at:
 *      https://gitlab.com/thirdlaw/starstruck/blob/master/LICENSE
 *//**
 *
 * A general library for HAL to allow robot control and controller input
 *
 * See individual library components for more information and for their interface
 */

 /**
 * @file
 * @author mrRachar
 */ // For all file contents to be documented

/**
 * @author mrRachar
 * @page rcs %Robot Control System
 *
 * @section intro_sec Introduction
 *
 * The R.C.S (%Robot Control System) provides an interface for robot motion and control
 * through data `struct`s and a simple functional interface
 *
 * @section design_sec Structure
 *
 * The robot control system is designed around configuration structs, namely `Robot` and
 * `Controller`, which hold the motor ports and input channels respectively. Functions
 * take these in and act on them, such as `go()` and `turn()`.
 *
 * The are also states, such as `ControllerState`, which can hold the current input values
 * for cleaner retrieval and can be updated using update functions (in this case
 * `updateControllerState()`).
 *
 * @subsection iface_sec Interface
 *
 * Data Structures:
 * - `Robot` - Motor ports configuration for a robot
 * - `Controller` - Input channels configuration for a controller
 * - `ControllerState` - The current (or last updated) input values from the controller channels
 *
 * Functions:
 * - `go(Robot robot, int x, int y)` - Control robot speed
 * - `goForwards(Robot robot, int y)` - Go only forwards
 * - `goSideways(Robot robot, int y)` - Go only sideways
 * - `turn(Robot robot, int speed)` - Rotate the robot
 * - `pushArm(Robot robot, int speed)` - Raise or lower the arm
 * - `pushFork(Robot robot, int speed)` - Extend or retract the forklift fork
 * - `standstill(Robot robot)` - Stop all robot motion
 * - `stopAll(Robot robot)` - Stop all robot motors, including those for motion and arms
 * - `updateControllerState(Controller controller, ControllerState* state)` -
 *                              Update this controller state to a controller's state
 * - `ponder(int milliseconds)` - Wait a given number of milliseconds
 *
 * Macros:
 * - `#constantly` - Repeat an action forever until the programme stops), equivalent to
 *                   `while(true)`
 */

#include "controller.h"
#include "robot.h"

#ifndef LIB_H_RCS
#define LIB_H_RCS
// Any general library code may go here, if any

/**
 * @def constantly
 * @author mrRachar
 * @see `updateControllerState(Controller controller, ControllerState* state)`
 *
 * Repeat a block of code forever (until the programme stops), equivalent to
 * `while(true)`
 *
 * Makes code clearer and less ugly. `while(true)` does show something is going to run
 * forever, but looks more "hacky" than `constantly`.
 *
 * How to use this macro:
 * @includelineno controller/controllerstate.c
 *
 * @example controller/controllerstate.c
 */
#define constantly while(true)

// Import guard
#endif
