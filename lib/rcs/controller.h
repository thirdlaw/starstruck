/***===========================================================***
 ***                                                           ***
 ***                  THIRD LAW ROBOTICS HAL                   ***
 ***               R.C.S. (Robot Control System)               ***
 ***                           v0.1.0                          ***
 ***                                                           ***
 ***===========================================================***
 *
 * This code is protected under the license available at:
 *      https://gitlab.com/thirdlaw/starstruck/blob/master/LICENSE
 *//**
 *
 * Simple controller channel data retrieval using a data-struct and optionally an
 * updatable state
 *
 * - `type struct Controller` - Save channel specific configuration
 * - `type struct ControllerState` - Current controller channel values
 * - `void updateControllerState(Controller controller, ControllerState* state)` - Update
 *                                     `state`'s values using controller configuration
 */

 /**
 * @file
 * @author mrRachar
 */ // For all file contents to be documented
#ifndef LIB_H_RCS_CONTROLLER_H
#define CONTROLLER_H

/**
 * @struct Controller
 * @author mrRachar
 * @see `ControllerState`
 *      `updateControllerState(Controller controller, ControllerState* state)`
 *
 * Store the current configuration of controller input channels
 *
 * Most of these channels should give a range of values (approx. -127 to 127) for the
 * motor speed to be set to, except for `forkExtend/Retract`, which should be a button
 * (i.e 1 or 0). These values can be used with `vexRT`, or better with
 * updateControllerState(), which will update a ControllerState with the latest incoming
 * values.
 *
 * Can be used with:
 * - `void updateControllerState(Controller controller, ControllerState* state)` -
 *                              Update a controller state to this controller's state
 *
 * How to construct a controller:
 * @includelineno controller/createcontroller.c
 *
 * @example controller/createcontroller.c
 *          controller/controllerstate.c
 */
typedef struct {
    /** The channel which decides the drag to turn the robot */
    TVexJoysticks driveSideways;
    /** The channel which decides the forwards/backwards motion */
    TVexJoysticks driveForwards;
    /** The channel which decides the rotational motion */
    TVexJoysticks driveTurning;
    /**
     * The channel which decides the speed of weapon compensated
     * lowering/raising
    */
    TVexJoysticks weaponHeight;
    /**
     * The button channel which decides if the fork should be pushed outwards from the
     * robot
     */
    TVexJoysticks forkExtend;
    /**
     * The button channel which decides if the fork should be pulled inwards towards the
     * robot
     */
    TVexJoysticks forkRetract;
    /**
     * The button channel which decides if the fork should be raised away from the
     * ground
     */
    TVexJoysticks armRaise;
    /**
     * The button channel which decides if the arm should be lowered towards the
     * ground
     */
    TVexJoysticks armLower;
    /**
     * The button channel which decides if the fork should be raised away from the
     * ground
     */
    TVexJoysticks pickupCubeEnable;
    /**
     * The button channel which decides if the arm should be lowered towards the
     * ground
     */
    TVexJoysticks pickupStarEnable;
} Controller;


/**
 * @struct ControllerState
 * @author mrRachar
 * @see `Controller`
 *      `updateControllerState(Controller controller, ControllerState* state)`
 *
 * The current (or last updated) input values from the controller channels, can be updated
 * by `updateControllerState()`
 *
 * Most of these values should be in the motor speed range (approx. -127 to 127) for the
 * motor speed to be set to, except for `fork`, which returns 1 if the fork is to  extend,
 * and -1 if the fork is to retract. This must be updated before each use with the update
 * function, otherwise the values will not be the latest.
 *
 * Can be used with:
 * - `void updateControllerState(Controller controller, ControllerState* state)` -
 *                              Update this controller state to a controller's state
 *
 * How to update the controller state:
 * @includelineno controller/controllerstate.c
 *
 * @example controller/controllerstate.c
 */
typedef struct {
    /**
     * The speed the robot should turn, by adding drag to the inside wheel and increasing
     * the speed of the outside wheels
     */
    int driveSideways;
    /** The speed the robot should be travelling at straight */
    int driveForwards;
    /** The speed the robot should be rotating. This will not work whilst the robot is
     * moving forwards/sideways
     */
    int driveTurning;
    /** The speed the weapon should be compensated lowering or raising */
    int weaponHeight;
    /**
    * -1 if the fork should extend, 1 if it should retract, 0 otherwise
    * (can be multiplied to give a speed)
    */
    int fork;
    /**
    * 1 if the arm should raise, -1 if it should lower, 0 otherwise
    * (can be multiplied to give a speed)
    */
    int arm;
    /**
    * 2 if it should pick up a cube, 1 if it should pick up a star, 0 otherwise
    */
    int pickupMode;
} ControllerState;


/**
 * @fn void updateControllerState(Controller controller, ControllerState* state)
 * @author mrRachar
 * @see `Controller`
 *      `ControllerState`
 *
 * Sets the current values to the controller state
 *
 * This takes the channel inputs from the controller and gets the values from `vexRT`, and
 * updates the current state to them. This is more useful than accessing them directly
 * from the `vexRT[]` array.
 *
 * @param controller    The channels the values should be mapped from
 * @param state         A pointer to a state to update in-place
 *
 * @returns `state` is ammended
 *
 * How to use this function:
 * @includelineno controller/controllerstate.c
 *
 * @example controller/controllerstate.c
 */
void updateControllerState(Controller controller, ControllerState* state){
    // Set the pointer's attributes to the incoming channel values in vexRT
    state->driveSideways = vexRT[controller.driveSideways];
    state->driveForwards = vexRT[controller.driveForwards];
    state->driveTurning = vexRT[controller.driveTurning];
    state->weaponHeight = vexRT[controller.weaponHeight];
    state->fork = vexRT[controller.forkRetract] - vexRT[controller.forkExtend];
    state->arm = vexRT[controller.armRaise] - vexRT[controller.armLower];
    state->pickupMode = (vexRT[controller.pickupCubeEnable]) ? 2 : vexRT[controller.pickupStarEnable];
}

// Import guard
#endif
