/***===========================================================***
 ***                                                           ***
 ***                  THIRD LAW ROBOTICS HAL                   ***
 ***               R.C.S. (Robot Control System)               ***
 ***                           v0.1.0                          ***
 ***                                                           ***
 ***===========================================================***
 *
 * This code is protected under the license available at:
 *      https://gitlab.com/thirdlaw/starstruck/blob/master/LICENSE
 *//**
 *
 * A library to allow robot movement control
 *
 * - `type struct Robot` - Save port specific configuration
 * - `void go(Robot robot, int x, int y)` - Get the robot to move
 * - `void goForwards(Robot robot, int y)` - Control forwards/backwards movement
 * - `void goSideways(Robot robot, int x)` - Control left/right movement
 * - `void turn(Robot robot, int speed)` - Allow robot rotation
 * - `void pushArm(Robot robot, int speed)` - Raise or lower a robot's arm
 * - `void pushFork(Robot robot, int speed)` - Extend or retract a robot's forklift fork
 * - `void standstill(Robot robot)` - Stop all a robot's motion
 * - `void stopAll(Robot robot)` - Stop all of a robot's motors, including those for
 *                                 motion and arms
 * - `int xSpeed(Robot robot)` - The x-direction speed of a given robot
 * - `int ySpeed(Robot robot)` - The y-direction speed of a given robot
 */

 /**
 * @file
 * @author mrRachar
 */ // For all file contents to be documented
#ifndef LIB_H_RCS_ROBOT_H
#define LIB_H_RCS_ROBOT_H

/**
 * @struct Robot
 * @author mrRachar
 *
 * Store the current configuration of robot motor ports
 *
 * This can be used to set motors by accessing attributes and retrieving the element
 * at that location from the `motor` array, such as: `motor[robot.frontLeftWheel]`
 *
 * Motion is controled through the functions:
 * - `void go(Robot robot, int x, int y)` - Control robot speed
 * - `void goForwards(Robot robot, int y)` - Go only forwards
 * - `void goSideways(Robot robot, int y)` - Go only sideways
 * - `void turn(Robot robot, int speed)` - Rotate the robot
 * - `void pushArm(Robot robot, int speed)` - Raise or lower the arm
 * - `void pushFork(Robot robot, int speed)` - Extend or retract the forklift fork
 * - `void standstill(Robot robot)` - Stop all robot motion
 * - `void stopAll(Robot robot)` - Stop all robot motors, including those for motion and
 *                                 arms
 * - `int xSpeed(Robot robot)` - The x-direction robot motor-speed
 * - `int ySpeed(Robot robot)` - The y-direction robot motor-speed
 *
 * How to construct a robot:
 * @includelineno robot/createrobot.c
 *
 * @example robot/createrobot.c
 *          robot/simpleautonomous.c
 */
typedef struct {
    /** The motor port which the front-left wheel is connected to */
    tMotor frontLeftWheel;
    /** The motor port which the front-right wheel is connected to */
    tMotor frontRightWheel;
    /** The motor port which the back-left wheel is connected to */
    tMotor backLeftWheel;
    /** The motor port which the back-right wheel is connected to */
    tMotor backRightWheel;
    /** The port the fork motor is plugged in to */
    tMotor fork;
    /** The port the left arm-lifting motor is plugged in to */
    tMotor leftArmLift;
    /** The port the right arm-lifting motor is plugged in to */
    tMotor rightArmLift;
} Robot;


/* Shout out to http://www.superdroidrobots.com/product_info/Mecanum-Wheel-Guide.pdf
   for a picture showing the mechanics */
/**
 * @fn void go(Robot robot, int x, int y)
 * @author mrRachar
 * @see `Robot`
 *      `goForwards(Robot robot, int y)`
 *      `goSideways(Robot robot, int x)`
 *      `turn(Robot robot, int speed)`
 *      `standstill(Robot robot)`
 *      `xSpeed(Robot robot)`
 *      `ySpeed(Robot robot)`
 *
 * Make the robot go forwards at speed y and sideways at speed x
 *
 * This will make the robot go a speed which is a conbination of the y component and the
 * x component. In theory, if x == 100 and y == 100, the robot should go north-east. This
 * was done with the help of a good diagramme available at:
 * http://www.superdroidrobots.com/product_info/Mecanum-Wheel-Guide.pdf
 *
 * It works by making the robot go the y speed forward, and then taking off any x speed
 * to affect the final velocity, which wheel velocities cancelling out in some directions
 * for the right overall speed, depending on the orientation of each wheel. The wheel set
 * -up should be like such in terms of cross-axis of the smaller wheels-on-the-wheels
 * (wheelception. (dun-dun-dun!)):
 * ```
 * \ /
 * / \
 * ```
 *
 * The directions are like on a standard representation of a cartesian plane
 *
 * @param robot     The robot configuration with which ports to change the speeds of
 * @param x         The speed to move in the x-direction. "+" → (east), "-" ← (west)
 * @param y         The speed to move in the y-direction. "+" ↑ (north), "-" ↓ (south)
 *
 * Simple autonomous code showing how movement functions can be used:
 * @includelineno robot/simpleautonomous.c
 *
 * @example robot/simpleautonomous.c
 */
void go(Robot robot, int x, int y){
    motor[robot.frontLeftWheel] = y + x;
    motor[robot.frontRightWheel] = y - x;
    motor[robot.backLeftWheel] = y - x;
    motor[robot.backRightWheel] = y + x;
}

/**
 * @fn void goForwards(Robot robot, int y)
 * @author mrRachar
 * @see `go(Robot robot, int x, int y)`
 *      `Robot`
 *
 * Make the robot go only forward at the given speed. Equivalent to `go(robot, x, 0)`.
 *
 * It exists to make your life easier. The 0 x speed arg seems a little annoying, and can
 * make code harder to read. `go(robot, 0, 100)`, what does that mean?
 * `goFowards(robot, 100)`, pretty clear! For more information, see `go()`.
 *
 * @param robot     The robot configuration with which ports to change the speeds of
 * @param y         The speed to move in the y-direction. "+" ↑ (north), "-" ↓ (south)
 *
 * @example robot/simpleautonomous.c
 */
void goForwards(Robot robot, int y){
    go(robot, 0, y);
}

/**
 * @fn void goSideways(Robot robot, int x)
 * @author mrRachar
 * @see go(Robot robot, int x, int y)
 *      Robot
 *
 * Make the robot go only sideways at the given speed. Equivalent to `go(robot, x, 0)`.
 *
 * It exists to make your life easier. The 0 y speed arg seems a little annoying, and can
 * make code harder to read. `go(robot, 100, 0)`, what does that mean?
 * `goSideways(robot, 100)`, pretty clear! For more information, see `go()`.
 *
 * @param robot     The robot configuration with which ports to change the speeds of
 * @param x            The speed to move in the x-direction. "+" → (east), "-" ← (west)
 */
void goSideways(Robot robot, int x){
    go(robot, x, 0);
}

/**
 * @fn void turn(Robot robot, int speed)
 * @author mrRachar
 * @see `go(Robot robot, int x, int y)`
 *      `Robot`
 *
 * Make the robot turn at the given speed. Positive is clockwise, negative anticlockwise
 *
 * This will make the robot rotate at the given speed, overwiting any forwards/sideways
 * speed. The mechanics behind the turning motions was worked out originally with the help
 * of a good diagramme available at:
 * http://www.superdroidrobots.com/product_info/Mecanum-Wheel-Guide.pdf
 *
 * It works by setting some of wheels in the positive direction and others in the
 * negative direction. The wheel set-up should be like such in terms of cross-axis of the
 * smaller wheels-on-the-wheels (wheelception. (dun-dun-dun!)):
 * ```
 * / \
 * \ /
 * ```
 *
 * Positive is clockwise. Negative is anticlockwise
 *
 * @param robot     The robot configuration with which ports to change the speeds of
 * @param speed     The speed to turn at. "+" ↷ (clockwise), "-" ↶ (anticlockwise)
 *
 * @example robot/simpleautonomous.c
 */
void turn(Robot robot, int speed){
    motor[robot.frontLeftWheel] = speed;
    motor[robot.frontRightWheel] = -speed;
    motor[robot.backLeftWheel] = speed;
    motor[robot.backRightWheel] = -speed;
}

/**
 * @fn void standstill(Robot robot)
 * @author mrRachar
 * @see `go(Robot robot, int x, int y)`
 *      `Robot`
 *      `stopAll(Robot robot)`
 *
 * Make the robot stop wheel motion. Equivalent to `go(robot, 0, 0)`.
 *
 * This will overwrite any other robot motion, making the robot not move at all. It will
 * not affect the fork or arms. It exists to make your life easier. The 0, 0 speed args
 * seem a little annoying, and can make code harder to read. `go(robot, 0, 0)`, what does
 * that mean? `standstill(robot)`,  pretty clear! For more information, see `go()`.
 *
 * @param robot     The robot configuration with which ports to stop
 *
 * @example robot/simpleautonomous.c
 */
void standstill(Robot robot){
    go(robot, 0, 0);
}

/**
 * @fn void pushArm(Robot robot, int speed)
 * @author mrRachar
 * @see Robot
 *
 * Push the robot arm up with both motors
 *
 * This will set the speed of both arm motors thus pushing the arm
 *
 * @param robot     The robot configuration with which ports to change the speeds of
 * @param speed     The speed to move the arm at. "+" ↑ (up), "-" ↓ (down)
 */
void pushArm(Robot robot, int speed){
    motor[robot.leftArmLift] = -speed;
    motor[robot.rightArmLift] = speed;
}

/**
 * @fn void pushFork(Robot robot, int speed)
 * @author mrRachar
 * @see `Robot`
 *
 * Extend or retract the robot forklift fork
 *
 * This will set the speed of motor at the end of the arm to retract or extend the robot's
 * fork.
 *
 * @param robot     The robot configuration with which ports to change the speeds of
 * @param speed     The speed to move the forklift at. "+" ↷ (extend), "-" ↶ (retract)
 */
void pushFork(Robot robot, int speed){
    motor[robot.fork] = speed;
}

/**
 * @fn void stopAll(Robot robot)
 * @author mrRachar
 * @see `go(Robot robot, int x, int y)`
 *      `Robot`
 *      `standstill(Robot robot)`
 *
 * Make all robot motors stop
 *
 * This will overwrite any other robot motor movement, making the robot motors not move at
 * all. It will affect the fork, arms, and wheels. It exists to make your life easier. To
 * do the same without this function would require calling at least three functions. Now
 * it's just one, making code a lot easier to read.
 *
 * @param robot     The robot configuration with which ports to stop
 */
void stopAll(Robot robot){
    go(robot, 0, 0);
    pushArm(robot, 0);
    pushFork(robot, 0);
}

/**
 * @fn int xSpeed(Robot robot)
 * @author mrRachar
 * @see `Robot`
 *      `go(Robot robot, int x, int y)`
 *
 * Get the speed of the robot in the *x*-direction
 *
 * This calculates the speed of the robot in the x-direction using the motor speed
 * settings and working backwards. This is *not the actual speed*, but the speed of the
 * motors in the x-direction.
 *
 * It's answer is calculated using `Robot::frontLeftWheel` and `Robot::frontRightWheel`
 * and thus if these are manually set it may not give sensible answers. If the robot is
 * turning it will return the average motor speed of `motor[Robot::frontLeftWheel]` and
 * `-motor[Robot::frontRightWheel]`, which will normally be the same as
 * `motor[Robot::frontLeftWheel]`.
 *
 * It was formulated by the following methodology:
 *
 * ```maths
 * motor[Robot::frontLeftWheel] = y + x
 * motor[Robot::frontRightWheel] = y - x
 *
 * => motor[Robot::frontLeftWheel] - x = y
 * => motor[Robot::frontRightWheel] = motor[Robot::frontLeftWheel] - x - x
 * => motor[Robot::frontRightWheel] - motor[Robot::frontLeftWheel] = -2x
 * => 2x = motor[Robot::frontLeftWheel] - motor[Robot::frontRightWheel]
 * ∴  x = 0.5 * (motor[Robot::frontLeftWheel] - motor[Robot::frontRightWheel])
 * ```
 *
 * @param robot     The robot configuration of which motors to get the speeds from
 * @returns         The motor speed in the x-direction
 */
int xSpeed(Robot robot){
    return 0.5 * (motor[robot.frontLeftWheel] - motor[robot.frontRightWheel]);
}

/**
 * @fn int ySpeed(Robot robot)
 * @author mrRachar
 * @see `Robot`
 *      `go(Robot robot, int x, int y)`
 *
 * Get the speed of the robot in the *y*-direction
 *
 * This calculates the speed of the robot in the y-direction using the motor speed
 * settings and working backwards. This is *not the actual speed*, but the speed of the
 * motors in the y-direction.
 *
 * It's answer is calculated using `Robot::frontLeftWheel` and `Robot::frontRightWheel`
 * and thus if these are manually set it may not give sensible answers. If the robot is
 * turning it will normally return 0.
 *
 * It was formulated by the following methodology:
 *
 * ```maths
 * motor[Robot::frontLeftWheel] = y + x
 * motor[Robot::frontRightWheel] = y - x
 *
 * => x = y - motor[Robot::frontRightWheel]
 * => motor[Robot::.frontLeftWheel] = y + y - motor[Robot::frontRightWheel]
 * => motor[Robot::frontLeftWheel] = 2y - motor[Robot::frontRightWheel]
 * => motor[Robot::frontRightWheel] + motor[Robot::frontLeftWheel] = 2y
 * ∴  y = 0.5(motor[Robot::frontRightWheel] + motor[Robot::frontLeftWheel])
 * ```
 *
 * @param robot     The robot configuration of which motors to get the speeds from
 * @returns         The motor speed in the y-direction
 */
int ySpeed(Robot robot){
    return 0.5 * (motor[robot.frontRightWheel] + motor[robot.frontLeftWheel]);
}

// Import guard
#endif
