// The variable `ROBOT` has already been defined and constructed
Controller CONTROLLER;

task controlLoop(){
    ControllerState state;
    constantly {
        // It is crucial to allways update the controller state.
        updateControllerState(CONTROLLER, state);
        go(ROBOT, state.driveSideways, state.driveForwards);
    }
}

task main(){
    //... Set up the CONTROLLER
}
