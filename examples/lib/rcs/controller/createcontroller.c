Controller CONTROLLER;

task main(){
    CONTROLLER.driveSideways = Ch4;
    CONTROLLER.driveForwards = Ch3;
    CONTROLLER.driveTurning = Ch1;
    CONTROLLER.armHeight = Ch2;
    CONTROLLER.forkExtend = Btn5D;
    CONTROLLER.forkRetract = Btn5U;
}
