Robot ROBOT;

task autonomousDrive(){
    goForwards(ROBOT, 100);
    ponder(1000);
    turn(ROBOT, 60);
    ponder(2000);
    go(ROBOT, 60, 100);
    standstill(ROBOT);
}

task main(){
    //... Set up the ROBOT
}
