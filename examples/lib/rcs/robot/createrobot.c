Robot ROBOT;

task main(){
    ROBOT.frontLeftWheel = motor2;
    ROBOT.frontRightWheel = motor3;
    ROBOT.backLeftWheel = motor4;
    ROBOT.backRightWheel = motor5;
    ROBOT.fork = motor7;
    ROBOT.leftArmLift = motor6;
    ROBOT.rightArmLift = motor9;
}
