/***===========================================================***
 ***                                                           ***
 ***                  THIRD LAW ROBOTICS HAL                   ***
 ***            A.M.C. (Autonomous Movement Control)           ***
 ***                           v0.0.0                          ***
 ***                                                           ***
 ***===========================================================***
 *
 * This code is protected under the license available at:
 *      https://gitlab.com/thirdlaw/starstruck/blob/master/LICENSE
 *//**
 *
 * Code to run autonomous movement
 */

#ifndef SRC_AUTONOMOUS_MAIN_H
#define SRC_AUTONOMOUS_MAIN_H

#include "../../lib/rcs/lib.h"
#include "../setup.h"

task autonomous(){
    //speed of fork movement
    int forkSpeed = 20;
    //move forward to fence
    goForwards(ROBOT, 120);
    sleep(1500);
    standstill(ROBOT);
    pushArm(ROBOT, 100);
    pushFork(ROBOT, -10);
    sleep(1500);
    pushFork(ROBOT, 0);
    pushArm(ROBOT, 0);
    constantly {
        pushFork(ROBOT, forkSpeed);
        sleep(500);
        pushFork(ROBOT, -forkSpeed);
        sleep(500);
    }
}

// Import guard
#endif
