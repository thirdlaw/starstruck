/***===========================================================***
 ***                                                           ***
 ***                  THIRD LAW ROBOTICS HAL                   ***
 ***               M.D.C (Manual Driving Control)              ***
 ***                           v0.1.0                          ***
 ***                                                           ***
 ***===========================================================***
 *
 * This code is protected under the license available at:
 *      https://gitlab.com/thirdlaw/starstruck/blob/master/LICENSE
 *//**
 *
 * A library to allow robot movement control
 */

 /**
 * @file
 * @author mrRachar
 */ // For all file contents to be documented

#ifndef SRC_DRIVER_MAIN_H
#define SRC_DRIVER_MAIN_H

#include "../../lib/rcs/lib.h"
#include "../setup.h"

int ARM_MOVEMENTSPEED = 90;
int FORK_MOVEMENTSPEED = 90;
float STAR_ADJUSTMENTCOEFFICIENT = 0.15;
float CUBE_ADJUSTMENTCOEFFICIENT = 0.05;

task usercontrol(){
    ControllerState state;
    int threshold = 15;
    constantly {
        // It is crucial to allways update the controller state.
        updateControllerState(CONTROLLER, state);

        // Set up sideways and forwards motion to see speed;
        int sidewaysMotion = 0;
        int forwardsMotion = 0;
        // Default to star adjust
        float weaponAjustmentCoefficient = STAR_ADJUSTMENTCOEFFICIENT;

        // Toggles
        if(state.pickupMode){
            if(state.pickupMode == 2){
                weaponAjustmentCoefficient = CUBE_ADJUSTMENTCOEFFICIENT;
            } else if(state.pickupMode == 1){
                weaponAjustmentCoefficient = STAR_ADJUSTMENTCOEFFICIENT;
            }
        }
        // Controls
        if(abs(state.driveSideways) > threshold){
            sidewaysMotion = state.driveSideways - threshold;
        }
        if(abs(state.driveForwards) > threshold){
            forwardsMotion = state.driveForwards - threshold;
        }
        if(sidewaysMotion || forwardsMotion){
            go(ROBOT, state.driveSideways, state.driveForwards);
        } else if(abs(state.driveTurning) > threshold){
            turn(ROBOT, state.driveTurning - threshold);
        } else {
            standstill(ROBOT);
        }
        if(abs(state.weaponHeight) > threshold){
            pushArm(ROBOT, state.weaponHeight);
            pushFork(ROBOT, -weaponAjustmentCoefficient * (state.weaponHeight + threshold));
        } else {
            if(state.fork){
                if(state.fork > 0) {
                    pushFork(ROBOT, FORK_MOVEMENTSPEED * state.fork);
                } else {
                    pushFork(ROBOT, FORK_MOVEMENTSPEED * 0.4 * state.fork);
                }
            } else {
                // Default value to auto-hold
                pushFork(ROBOT, 10);
            }
            if(state.arm){
                if(state.arm > 0) {
                  pushArm(ROBOT, ARM_MOVEMENTSPEED * state.arm);
                } else {
                  pushArm(ROBOT, ARM_MOVEMENTSPEED * 0.4 * state.arm);
                }
            } else {
                // Default value to auto-hold
                pushArm(ROBOT, 10);
            }
        }
    }
}

// Import guard
#endif
