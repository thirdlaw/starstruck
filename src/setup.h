/***===========================================================***
 ***                                                           ***
 ***                  THIRD LAW ROBOTICS HAL                   ***
 ***                           v0.0.0                          ***
 ***                                                           ***
 ***===========================================================***
 *
 * This code is protected under the license available at:
 *      https://gitlab.com/thirdlaw/starstruck/blob/master/LICENSE
 *//**
 *
 * A library to allow robot movement control
 */

#ifndef SRC_SETUP_H
#define SRC_SETUP_H

#include "../lib/rcs/lib.h"

Controller CONTROLLER;
Robot ROBOT;

void setup(){
    // Construct robot
    ROBOT.frontLeftWheel = port2;
    ROBOT.frontRightWheel = port8;
    ROBOT.backLeftWheel = port3;
    ROBOT.backRightWheel = port7;
    ROBOT.fork = port5;
    ROBOT.leftArmLift = port4;
    ROBOT.rightArmLift = port6;

    // Construct controller
    CONTROLLER.driveSideways = Ch4;
    CONTROLLER.driveForwards = Ch3;
    CONTROLLER.driveTurning = Ch1;
    CONTROLLER.weaponHeight = Ch2;
    CONTROLLER.forkExtend = Btn5D;
    CONTROLLER.forkRetract = Btn5U;
    CONTROLLER.armRaise = Btn6U;
    CONTROLLER.armLower = Btn6D;
    CONTROLLER.pickupStarEnable = Btn8D;
    CONTROLLER.pickupCubeEnable = Btn8R;

    // Reverse the motors that are physically reversed
    bMotorReflected[ROBOT.frontRightWheel] = true;
    bMotorReflected[ROBOT.backRightWheel] = true;
}

void pre_auton(){
    setup();
}

#endif
