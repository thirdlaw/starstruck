/***===========================================================***
 ***                                                           ***
 ***                  THIRD LAW ROBOTICS HAL                   ***
 ***                           v0.0.0                          ***
 ***                                                           ***
 ***===========================================================***
 *
 * This code is protected under the license available at:
 *      https://gitlab.com/thirdlaw/starstruck/blob/master/LICENSE
 *//**
 *
 * A library to allow robot movement control
 */

#include "Vex_Competition_Includes.c" // Main competition background code

#include "./setup.h"
#include "../lib/rcs/lib.h"
#include "./driver/main.h"
#include "./autonomous/main.h"

#pragma platform(VEX)

#pragma competitionControl(Competition)
#pragma autonomousDuration(15)
#pragma userControlDuration(95)

// No main code
