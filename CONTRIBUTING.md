# Contributor's Guide

Please see the [official style guide](https://docs.google.com/document/d/1zGY4MgZiKlTeoJG-Bm6C3at2n1vVXlz83TqmQ5fZ3tQ/) for code styling practice.

Could all of us please contribute to a branch labelled by the part of the project you are working on, and then submit a pull request when you've finished what you're been working on. This way, code problems should arise less often, and there is less risk of someone overwriting what you've been working on.

Also, do look at other peoples branches, and check out what they're doing. They may be working on what you need. **Do not be afraid to submit frequent pull requests**, so that the master branch stays normally up-to-date, people don't do the same thing someone else has done, and **so we can make sure things work together well earlier, rather than waiting too late for problems between people's code to arise.**

## Setting Up

The following instructions may not be what you are told to do online, however, they will set git up in such a way to force GitLab to work well with Atom, or other text editors.

_For Windows:_

 - Download [Git Credential Manager for Windows](https://github.com/Microsoft/Git-Credential-Manager-for-Windows). This will install git 2.10 if you haven't already got it.
If you've already installed git before, you _may (or not)_ have to [set up your PATH in the environmental variables](http://superuser.com/questions/949560/how-do-i-set-system-environment-variables-in-windows-10) to put git 2.10 before any other git installations
 - Open _Git Bash_ and run `git config --global credential.helper wincred`
 - To start working, run `git clone https://gitlab.com/thirdlaw/starstruck.git`. *You should be prompted for a username and password. These are your GitLab credentials.*

If you have any problems, see this great [S.O. answer](http://stackoverflow.com/questions/5343068/is-there-a-way-to-skip-password-typing-when-using-https-on-github)

To set up this git repo with  [Atom](http://atom.io):

 - Install the [language-robotc](https://atom.io/packages/language-robotc) package for correct RobotC syntax highlighting. You can install packages from Settings _(press_ <kbd>cntrl</kbd>  _+_ <kbd>, (comma)</kbd>_)_
 - Open the local clone by finding the folder it was downloaded to, and right clicking, and press 'open with Atom'. If you do not see this option, go into Atom, press <kbd>alt</kbd>, and select 'Open Folder', then choose the folder.
 - Using [git-control](https://atom.io/packages/git-control) you can add, commit and push what you want to contribute. **Please create your own branch and submit a pull request to the master branch when you're ready.**

If you do not understand some or any of this guide, please ask!
