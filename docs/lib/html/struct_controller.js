var struct_controller =
[
    [ "armLower", "struct_controller.html#aabce29b6ee07e253a7b52e156819bda1", null ],
    [ "armRaise", "struct_controller.html#a199a619eba5a3d3520d250b6624440f5", null ],
    [ "driveForwards", "struct_controller.html#a7f297d23287673e6c5aab01637f7e37d", null ],
    [ "driveSideways", "struct_controller.html#a1f0cf5f0a8483d972b5198c58eaf2a75", null ],
    [ "driveTurning", "struct_controller.html#a94c16ae7a1b6b2c91cefa465a65ba178", null ],
    [ "forkExtend", "struct_controller.html#a43a39882852cc5264e8da97522dba681", null ],
    [ "forkRetract", "struct_controller.html#a8b98f83cac06e1a5fbff56eafa92d331", null ],
    [ "pickupCubeEnable", "struct_controller.html#a38b3322ab32124315f34f43ec7846366", null ],
    [ "pickupStarEnable", "struct_controller.html#a1a7e288243d9fc89bf15583f95aae071", null ],
    [ "weaponHeight", "struct_controller.html#a880c27895a8666b491f56c369aea85b9", null ]
];