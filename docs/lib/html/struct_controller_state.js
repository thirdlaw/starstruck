var struct_controller_state =
[
    [ "arm", "struct_controller_state.html#ae2a4ce8da9c0ac53697d563f90148c9f", null ],
    [ "driveForwards", "struct_controller_state.html#aedf8f32cd74a322a9aa67a7c868090f7", null ],
    [ "driveSideways", "struct_controller_state.html#a454abd5b75dc92c6f2fb84bd7bd0d4e8", null ],
    [ "driveTurning", "struct_controller_state.html#a937671f7300a0dcb43b31beecac23da9", null ],
    [ "fork", "struct_controller_state.html#a88db5a8f4cc570c90c6d56ced1f8e3b1", null ],
    [ "pickupMode", "struct_controller_state.html#afab2b1283606f59e409f60d9b76dc084", null ],
    [ "weaponHeight", "struct_controller_state.html#acb280008ea7dfe66acc2d023ef80265f", null ]
];