var robot_8h =
[
    [ "Robot", "struct_robot.html", "struct_robot" ],
    [ "go", "robot_8h.html#afc0ab4a49e687a925852e204bf082559", null ],
    [ "goForwards", "robot_8h.html#a335ae34b559c7da0a23270e056d07a71", null ],
    [ "goSideways", "robot_8h.html#a91eca06744e9431b002b76caf3b038d1", null ],
    [ "pushArm", "robot_8h.html#a2bfcc937daf30ee84a632ec210f646e5", null ],
    [ "pushFork", "robot_8h.html#aa5da0cac1e5c293ed1c7ad1afb787bc5", null ],
    [ "standstill", "robot_8h.html#aa748342f43db3d3da7d63722b1121330", null ],
    [ "stopAll", "robot_8h.html#a48764dcdda8eba5fb251c89344c9108a", null ],
    [ "turn", "robot_8h.html#afa7b0fd026a1725aeacc70644964c9dd", null ],
    [ "xSpeed", "robot_8h.html#a07d835b5755585d0223fe7dc7e4c183c", null ],
    [ "ySpeed", "robot_8h.html#aa1104c337204b82a854b098f5e821fe2", null ]
];