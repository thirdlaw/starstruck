var struct_robot =
[
    [ "backLeftWheel", "struct_robot.html#af9e48aa311eb1e32e34b4ba0d0499074", null ],
    [ "backRightWheel", "struct_robot.html#a7f09f26b03d03e48a7d6505d1ef7688f", null ],
    [ "fork", "struct_robot.html#aafde078f4b56c9bbdffc63f5dc5a5d37", null ],
    [ "frontLeftWheel", "struct_robot.html#ac210d302ea7c3f37f29eab8830a654d7", null ],
    [ "frontRightWheel", "struct_robot.html#a27db9e1adaac7047db211db92579a5f5", null ],
    [ "leftArmLift", "struct_robot.html#a935c792ab2c619f4c421161cad1b643a", null ],
    [ "rightArmLift", "struct_robot.html#a4f26ba00f61819a4770a8ff04bfa4ab4", null ]
];