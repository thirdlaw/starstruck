<div align="center"><img src='http://thirdlawonly.com/rsc/eyeplaque.png' width="20%" height="auto"/><br/><br/></div>

<div align="center">
    <p>Third Law Only <strong>HAL 9001</strong> Robot Codebase<br/></p>
    <p><em><a href="http://hal.thirdlawonly.com/docs/lib/v0_0_0/">Read the Documentation</a></em><br/></p>
    <p>with thanks to Matthew Ross Rachar and Navyaa Mathur</p>
</div>

## About

  We're taking part in the VEX Robotics 2016 Starstruck competition with our own amazing
robot called HAL 9001. To make this robot operate *awesomely*, we've been writing a
library and a application which you can find here in this repository.

## Structure

  The code is split up into two parts, the library and the application. The library
contains a simple robot programming interface to interact with, and the application uses
this, sets things up and controls the autonomous period and manual robot control. Both of
these are reasonably well documented, although the library
[slightly better so](http://hal.thirdlawonly.com/docs/lib/v0_0_0/).


### Library

  The library uses structs and functions to provide a simple programming interface to
interact with the robot. For example it turns this:

```c
motor[port2] = 100
motor[port3] = 100
motor[port4] = -100
motor[port5] = -100
```

into:

```c
// After having constructed a Robot struct instance
turn(robot, 100)
```

  It has many more functions which allow the same simple interaction. There is also an
autonomous library underway which should allow distances and rotations to be provided,
instead of motor speeds.

*[Read the Documentation](http://hal.thirdlawonly.com/docs/lib/v0_0_0/)*

## Open Source

  All our code will be *mostly* open-source during and after the competitions, but not
before them as we work on it, and don't want people to get a free ride. You can read the
[license](LICENSE) which basically states
that you can't go claiming it was your own, you can't go changing the license, and we're
not responsible if it doesn't work, or breaks lots of things, as it is your responsibility
to read through the code so you understand the risks.
